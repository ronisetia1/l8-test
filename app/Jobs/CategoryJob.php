<?php

namespace App\Jobs;

use App\Models\User;
use App\Models\Category;
use Illuminate\Bus\Queueable;
use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class CategoryJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $category;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Category $category)
    {
        $this->category = $category;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $category = $this->category;
        $users = User::all();

        foreach ($users as $user) {
            Mail::send([], [], function ($message) use ($user, $category) {
                $message->from("ronisetiawan1099@gmail.com")
                    ->to($user->email)
                    ->subject('Notify category')
                    ->setBody("Hi, welcome {$user->name}!")
                    ->setBody("<h1>Ini adalah category {$category->name}</h1>", 'text/html');
            });
        }
    }
}
