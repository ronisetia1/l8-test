<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;

class ApiCategoryController extends Controller
{
    public function index()
    {
        $category = Category::latest()->paginate(10);
        return [
            "status" => 1,
            "data" => $category
        ];
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'is_publish' => 'boolean'
        ]);

        $category = Category::create($request->all());
        return [
            "status" => 1,
            "data" => $category
        ];
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'is_publish' => 'boolean'
        ]);

        $category = Category::FindOrFail($id)->update([
            'name' => $request->name,
            'is_publish' => $request->is_publish,
        ]);

        return [
            "status" => 1,
            "data" => $category,
            "msg" => "Data Successfully Updated"
        ];
    }

    public function destroy(Category $category)
    {
        $category->delete();
        return [
            "status" => 1,
            "data" => $category,
            "msg" => "Data Successfully Deleted",
        ];
    }
}
