<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CategoryController extends Controller
{
    public function index()
    {
        $kategori = DB::table('categories')->paginate(10);
		return view('category.index',['categories' => $kategori]);
    }

    public function cari(Request $request)
    {
        $cari = $request->cari;

		$kategori = DB::table('categories')
		->where('name','like',"%".$cari."%")
		->paginate();

		return view('category.index',['categories' => $kategori]);
    }

    public function create()
    {
        return view('category.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'is_publish' => 'boolean'
        ]);

        $kategori = Category::create([
            'name' => $request->name,
            'is_publish' => $request->is_publish,
        ]);

        if($kategori){
            //redirect dengan pesan sukses
            return redirect()->route('category.index')->with(['success' => 'Data Berhasil Disimpan!']);
        }else{
            //redirect dengan pesan error
            return redirect()->route('category.index')->with(['error' => 'Data Gagal Disimpan!']);
        }
    }

    public function edit($id)
    {
        $category = DB::table('categories')->where('id', $id)->first();
        return view('category.edit',['category' => $category]);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name'     => 'required',
            'is_publish'   => 'boolean'
        ]);

        $category = Category::FindOrFail($id)->update([
            'name' => $request->name,
            'is_publish' => $request->is_publish,
        ]);

        if($category){
            //redirect dengan pesan sukses
            return redirect()->route('category.index')->with(['success' => 'Data Berhasil Disimpan!']);
        }else{
            //redirect dengan pesan error
            return redirect()->route('category.index')->with(['error' => 'Data Gagal Disimpan!']);
        }
    }

    public function delete($id)
    {
        $category = Category::FindOrFail($id);
        $category->delete();

        if($category){
            //redirect dengan pesan sukses
            return redirect()->route('category.index')->with(['success' => 'Data Berhasil Dihapus!']);
         }else{
           //redirect dengan pesan error
           return redirect()->route('category.index')->with(['error' => 'Data Gagal Dihapus!']);
         }
    }
}
