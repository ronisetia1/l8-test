<!DOCTYPE html>
<html>
<head>
	<title>Data Kategori</title>
</head>
<body>

	<style type="text/css">
		.pagination li{
			float: left;
			list-style-type: none;
			margin:5px;
		}
	</style>

	<h3>Data Kategori</h3>

	<p>Cari Data Kategori :</p>
	<form action="/category/cari" method="GET">
		<input type="text" name="cari" placeholder="Cari Kategori .." value="{{ old('cari') }}">
		<input type="submit" value="CARI">
	</form>

	<br/>
    <a href="{{ route('category.create') }}" class="btn btn-md btn-success mb-3">TAMBAH KATEGORI</a>
	<table border="1">
		<tr>
			<th>Nama</th>
			<th>Is Publish</th>
            <th>Aksi</th>
		</tr>
		@foreach($categories as $category)
		<tr>
			<td>{{ $category->name }}</td>
			<td>{{ $category->is_publish }}</td>
            <td class="text-center">
                <form onsubmit="return confirm('Apakah Anda Yakin ?');" action="{{ route('category.delete', $category->id) }}" method="POST">
                    <a href="{{ route('category.edit', $category->id) }}" class="btn btn-sm btn-primary">EDIT</a>
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-sm btn-danger">HAPUS</button>
                </form>
            </td>
		</tr>
		@endforeach
	</table>

	<br/>
	Halaman : {{ $categories->currentPage() }} <br/>
	Jumlah Data : {{ $categories->total() }} <br/>
	Data Per Halaman : {{ $categories->perPage() }} <br/>


	{{ $categories->links() }}

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</body>
</html>
