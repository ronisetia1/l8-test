<?php

use App\Http\Controllers\ApiCategoryController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/category', [ApiCategoryController::class, 'index']);
Route::get('/category/cari', [ApiCategoryController::class, 'cari']);
Route::get('/category/create', [ApiCategoryController::class, 'create']);
Route::post('/category/store', [ApiCategoryController::class, 'store']);
Route::get('/category/edit/{id}', [ApiCategoryController::class, 'edit']);
Route::put('/category/update/{id}', [ApiCategoryController::class, 'update']);
Route::delete('/category/delete/{id}', [ApiCategoryController::class, 'delete']);
