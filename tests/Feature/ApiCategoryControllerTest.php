<?php

namespace Tests\Feature;

use App\Models\Category;
use Faker\Factory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ApiCategoryControllerTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testCreateCategory()
    {
        $category = [
            "name" => "Anggiat",
            "is_publish" => "1",
        ];

        $this->json('POST', 'api/category', $category, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                "category" => [
                    "name" => "Anggiat",
                    "is_publish" => "1",
                ],
                "message" => "Created successfully"
            ]);
    }

    public function testUpdateCategory()
    {
        $category = Factory(Category::class)->create([
            "name" => "Junnaidi",
            "is_publish" => "1",
        ]);

        $payload = [
            "name" => "Jarot",
            "is_publish" => "0",
        ];

        $this->json('PUT', 'api/category/' . $category->id , $payload, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                "category" => [
                    "name" => "Jarot",
                    "is_publish" => "0",
                ],
                "message" => "Updated successfully"
            ]);
    }

    public function testDeleteCategory()
    {
        $user = Factory(Category::class)->create();
        $this->actingAs($user, 'api');

        $category = Factory(Category::class)->create([
            "name" => "Junnaidi",
            "is_publish" => "1",
        ]);

        $this->json('DELETE', 'api/category/' . $category->id, [], ['Accept' => 'application/json'])
            ->assertStatus(204);
    }
}
